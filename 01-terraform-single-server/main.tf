terraform {
 required_version = ">=0.12, <0.13"
}

provider "aws" { 
    region = "ap-south-1" 
} 
resource "aws_instance" "ec2_instance" { 
    ami = "ami-04b2519c83e2a7ea5" 
    instance_type = "t2.micro"
    availability_zone = "ap-south-1a"
    vpc_security_group_ids = [aws_security_group.web_security.id]
    key_name = "kops-keypair"
    user_data = <<-EOF
                    #!/bin/bash
                    sudo yum update -y
                    sudo yum install nginx -y 
                    sudo service nginx start
                EOF
    tags = {
        Name = "nginx-instance",
        created-date = "22-04-2020"
    }
} 

resource "aws_security_group" "web_security" {
    name = var.security_group

  # can be deleted after you confirm
  ingress {
     from_port = var.ssh_port_number
     to_port = var.ssh_port_number
     protocol = "tcp" 
     cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
     from_port = var.http_port_number
     to_port = var.http_port_number
     protocol = "tcp" 
     cidr_blocks = ["0.0.0.0/0"]
  } 
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

variable "security_group" {
    description = "name of the security group"
    type = string
    default = "web_security_group"
}

variable "http_port_number" {
    description = "port number for nginx"
    type = number
    default = 80
}
variable "ssh_port_number" {
    description = "port number for ssh login"
    type = number
    default = 22
}

output "web_server_details" {
  value = aws_instance.ec2_instance.public_ip
  description = "public IP address of the web server"
}
