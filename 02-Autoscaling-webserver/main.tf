provider "aws" {
  profile = "pradeep"
  region = "ap-south-1" # Change this to your desired region
}


resource "aws_launch_configuration" "example_lc" {
  name_prefix = "example-lc-"
  image_id    = "ami-0a7a85e9ba0ff5ad3"  # Replace with your desired AMI ID
  instance_type = "t2.micro"

  user_data = <<-EOF
              #!/bin/bash
              echo "Hello from User Data!"
              EOF
}

resource "aws_autoscaling_group" "example_asg" {
  name                 = "example-asg"
  min_size             = 2
  max_size             = 4
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.example_lc.name
  availability_zones  = ["ap-south-1a", "ap-south-1b"]  # Replace with desired AZs

  tag {
    key                 = "Name"
    value               = "pradeep-example-instance"
    propagate_at_launch = true
  }
}
