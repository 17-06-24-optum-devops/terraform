provider "aws" {
  profile = "terraform"
  region = "ap-south-1" # Change this to your desired region
}

terraform {
    backend "s3"{
        bucket = "classpath-application-state"
        key = "global/s3/terraform.tfstate"
        region = "ap-south-1"
        dynamodb_table = "terraform-managed-lock-table"
        encrypt = true
    }
}

#create a DynamoDB for locking the terraform state
resource "aws_dynamodb_table" "terraform_lock_table" {
    name = "terraform-managed-lock-table"
    billing_mode = "PAY_PER_REQUEST"
    hash_key = "LockID"
    attribute {
        name = "LockID"
        type = "S"
    }
}

output "dynamodb_table_name" {
  value = aws_dynamodb_table.terraform_lock_table.name
  description = "Database table name"
}

