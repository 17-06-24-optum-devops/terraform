# terraform {
#     backend "s3"{
#         bucket = "classpath-application-state"
#         key = "global/s3/terraform.tfstate"
#         region = "ap-south-1"
#         dynamodb_table = "terraform-managed-lock-table"
#         encrypt = true
#     }
# }

provider "aws" {
  region = "ap-south-1"
}


resource "aws_s3_bucket" "application_state" {
  bucket = "classpath-application-state"

  #Prevent from accidentally deleting the bucket
  lifecycle {
      prevent_destroy = true
  }

  #enable versioning
  versioning {
      enabled = true
  }

  #enable server side encryption
  server_side_encryption_configuration {
      rule {
          apply_server_side_encryption_by_default {
              sse_algorithm = "AES256"
          }
      }
  }
}

#create a DynamoDB for locking the terraform state
resource "aws_dynamodb_table" "terraform_lock_table" {
    name = "terraform-managed-lock-table"
    billing_mode = "PAY_PER_REQUEST"
    hash_key = "LockID"
    attribute {
        name = "LockID"
        type = "S"
    }
}


