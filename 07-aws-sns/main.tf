provider "aws" {
  profile = "terraform"  
  region = "ap-south-1"
}

resource "aws_sns_topic" "pradeep-demo-topic" {
  name = "my-topic"
  
  display_name = "test topic"
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect = "Allow",
      Principal = "*",
      Action = "SNS:Publish",
      Resource = "*"
    }]
  })
}

output "topic_arn" {
  value = aws_sns_topic.pradeep-demo-topic.arn
}

output "sns_console_url" {
  value = "https://console.aws.amazon.com/sns/v3/home?region=ap-south-1#/topics/${aws_sns_topic.pradeep-demo-topic.name}"
  description = "Link to SNS topic in AWS Management Console"
}
