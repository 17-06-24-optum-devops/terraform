provider "aws" {
  profile = "terraform"
  region  = "ap-south-1"  # Replace with your desired region
}

resource "aws_dynamodb_table" "employee_table" {
  name           = "Employee"
  billing_mode   = "PAY_PER_REQUEST"

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "name"
    type = "S"
  }

  attribute {
    name = "age"
    type = "N"
  }

  attribute {
    name = "salary"
    type = "N"
  }

  hash_key = "id"
  range_key = "name"

  global_secondary_index {
    name = "age-index"
    hash_key = "age"
    projection_type = "ALL"
  }

  global_secondary_index {
    name = "salary-index"
    hash_key = "salary"
    projection_type = "ALL"
  }
}

output "dynamodb_table_details" {
  value = {
    arn  = aws_dynamodb_table.employee_table.arn,
    name = aws_dynamodb_table.employee_table.name,
  }
}
