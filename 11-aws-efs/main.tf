provider "aws" {
  profile = "terraform"
  region  = "ap-south-1"
}

resource "aws_efs_file_system" "example_efs" {
  creation_token = "example-efs"  # Replace with a unique name
  performance_mode = "generalPurpose"
  throughput_mode = "bursting"
}

data "aws_subnets" "example" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }
}



data "aws_vpc" "example_vpc" {
  default = true
}

variable "vpc_id" {
  description = "ID of the VPC where EFS will be created"
}

resource "aws_security_group" "example_efs_security_group" {
  name        = "example-efs-security-group"
  description = "Security group for EFS"
  
  ingress {
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_efs_mount_target" "example_mount_target" {
  count          = length(data.aws_subnets.example.ids)
  file_system_id = aws_efs_file_system.example_efs.id
  subnet_id      = element(data.aws_subnets.example.ids, count.index)
  security_groups = [aws_security_group.example_efs_security_group.id]
}

output "efs_dns_name" {
  value = aws_efs_file_system.example_efs.dns_name
}
